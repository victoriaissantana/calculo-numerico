package calculonumerico;

import org.ejml.simple.SimpleMatrix;

import java.util.ArrayList;
import java.util.Scanner;

import static java.lang.Math.log;
import static java.lang.Math.pow;

import java.awt.Graphics2D;

/**
 * @author Victoria Santana
 *
 * Mostremos, agora, como ajustar um conjunto de pontos(xi, yi)a uma exponencial do tipo y = α e^bx.
 * baseado no material: http://www.decom.ufop.br/prof/marcone/Disciplinas/MetodosNumericoseEstatisticos/QuadradosMinimos.pdf
 * */
public class Main {

    public static void imprimirResultados(double x[]) {
        System.out.println("Incógnitas:" + "\r\n");
        for (int i = 0; i < x.length; ++i) {

            System.out.println("x[" + i + "] = " + x[i]);

        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

	    System.out.println("Digite o numero de valores de pares x,y");
	    int numeroPares = scanner.nextInt();

        ArrayList<Ponto> vetorPares = new ArrayList<Ponto>(numeroPares);

        String x, y;


        for(int i = 0; i < numeroPares; i++){
            System.out.println("Digite o valor do par na posição "+ i);

            x = scanner.next();

            y = scanner.next();

            float x1 = Float.parseFloat(x);
            float y1 = Float.parseFloat(y);

            Ponto ponto = new Ponto(x1,y1);

            vetorPares.add(ponto);

        }

        //Transformação de Y em Logaritmo Natural de Y

        for(Ponto p : vetorPares){
            p.setY((float) log(p.getY()));
        }

        //Debug, ver x e y

        for( Ponto p : vetorPares){
            System.out.print(p.getX());
            System.out.println(" " + p.getY());
        }

        // Somatorio de x, de x ², de y e xy
        float somaX = 0, somaXX = 0, somaY = 0, somaXY = 0;

        for(int i = 0; i < numeroPares; i++){
            somaX = somaX + vetorPares.get(i).getX();
            somaXX = (float) (somaXX + pow(vetorPares.get(i).getX(),2));
            somaY = somaY + vetorPares.get(i).getY();
            somaXY = (float) (somaXY + (vetorPares.get(i).getX() * vetorPares.get(i).getY()));
        }

        //debug, ver os somatórios

        System.out.println("Soma de x: "+ somaX);
        System.out.println("Soma de x²: "+ somaXX);
        System.out.println("Soma de y: "+ somaY);
        System.out.println("Soma de xy: "+ somaXY);

        // as equações normais do problema são
        //  numeroPares * a + somaX * b = somaY
        // somaX * a + somaXX * b = somaXY

        double[][] matrizA = new double[2][2];
        double[] termosInd = new double[2];

        matrizA = new double[][]{{numeroPares, somaX}, {somaX, somaXX}};
        termosInd = new double[]{somaY, somaXY};


       LU solve = new LU();

       double X[] = solve.resolve(matrizA, termosInd);

       imprimirResultados(X);

       //descubro o alfa para fazer a equação

        float alfa = (float) Math.exp(X[0]);

        System.out.println("A exponencial que mais se adequa a tabela eh: "+ alfa + "e elevado a "+ X[1] +"x");

        //gerando graficos

        //coeficiente de correlação de pearson

        

    }
}
